#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#Export
export TERM="xterm-256color"
export HISTCONTROL=ignoredups:erasedups
export HISTTIMEFORMAT="%h %d %H:%M:%S"
export HISTSIZE=1000
export EDITOR="vim"

#Shopt
shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s histappend
shopt -s expand_aliases
shopt -s checkwinsize

#Ignore case when TAB completion
bind "set completion-ignore-case on"

#Aliases
alias ls='exa --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias pacs='doas pacman -S'
alias pacss='doas pacman -Ss'
alias pacsyu='doas pacman -Syu'
alias pacsyyu='doas pacman -Syyu'
alias yaysyu='yay -Sua --noconfirm'
alias yays='yay -S --noconfirm'
alias yayss='yay -Ss --noconfirm'
alias yayrns='yay -Rns --noconfirm'
alias pacrns='doas pacman -Rns '
alias paccache='doas pacman -Sc'
alias paclock='doas rm /var/lib/pacman/db.lck'
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias sudo='doas'
alias vi='vim'
alias rtfm='la /usr/share/doc/arch-wiki/html/en'
alias usbwatch='usbguard watch'
alias usballow='usbguard allow-device'
alias usbgenerate='usbguard generate-policy > /etc/usbguard/rules.conf'
alias storemod='modprobed-db store'
alias listmod='modprobed-db list'
alias listytdl='yt-dlp --list-formats'
alias ytdl='yt-dlp -f best'
alias ytdla='yt-dlp -x --audio-format flac'
alias dockerimages='doas docker images'
alias dockerps='doas docker ps'
alias dockerun='doas docker run -it -d'
alias dockerexec='doas docker exec -it'
alias gauth='head -n 1 .google_authenticator | xargs oathtool --totp -b $1'
alias authy='authy --no-sandbox'
#alias usermod -G='usermod -aG'

#Terminal
PS1='[\u@\h \W]\$ '
set -o vi
neofetch
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh
