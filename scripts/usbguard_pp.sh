#!/bin/bash

#USBGuard activation script

source ./bash_colors

echo -e "${BLUE}Checking Installation of USBGuard Software...${NC}"
for name in usbguard
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
      then
          echo -e "${GREEN}Already Installed:  $name${NC}"
          break
    elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
      then
           pacman -S --noconfirm --needed $name
          if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
            then
            echo -e "${GREEN}Installed:  $name${NC}"
            break
          else
            echo -e "${RED}Check Installation of Depenedencies${NC}"
            exit 1
          fi
    else
          echo -e "${RED}$name was not installed.${NC}"
          exit 1
    fi
done
echo
echo -e "${BLUE}Generating Default Policy...${NC}"
usbguard generate-policy > /etc/usbguard/rules.conf
if grep "allow id" /etc/usbguard/rules.conf 
    then
    echo -e "${GREEN}Default Policy Generated${NC}"
else
    echo -e "${RED}Default Policy Not Generated...."
    echo -e "Please Generate a Default Policy${NC}"
    exit 1
fi
echo
echo -e "${BLUE}IPCAllowedUsers${NC}"
grep IPCAllowedUsers /etc/usbguard/usbguard-daemon.conf
echo
echo -e "${BLUE}Is "root" an Allowed User and Has a Default Policy been Generated?${NC}"
select policy in Yes No
do 
    case $policy in 
        Yes)
            systemctl enable usbguard.service
            systemctl start usbguard.service
            break;;
        No)
            echo -e  "${RED}Default Policy Not Generated...."
            echo -e "Please Generate a Default Policy!${NC}"
            exit 1;;
        *)
            echo -e "${RED}Invalid Input${NC}"
            exit 1;;

    esac
done
echo -e "${GREEN}USB Guard Enabled${NC}"
