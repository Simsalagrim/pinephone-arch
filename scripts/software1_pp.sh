#!/bin/bash

#Package install script no.1

source ./bash_colors

echo -e "${BLUE}Initial Package Installation...${NC}"
for name in vim base-devel opendoas firefox cmake git curl wget \
 man-db man-pages pam go nftables iptables-nft
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
    elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           sudo pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
                  echo -e "${GREEN}Installed:  $name${NC}"
            else
                  echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
    else
        echo -e "${RED}$name was not installed.${NC}"
    fi
done  
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
