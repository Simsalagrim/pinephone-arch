#!/bin/bash

#Updates Unbound Conf to Set Values

source ./bash_colors

priv=doas
rootdir=/etc/unbound
ogconf=$rootdir/unbound.conf
newconf=$ogconf.pacnew
oldconf=$ogconf.old

echo -e "${BBLUE}Starting Update of Unbound.conf${NC}"
echo -e "${BLUE}Proposed changes to unbound.conf${NC}"
diff $ogconf $newconf
echo
echo -e "${GREEN}Updating Unbound.conf...${NC}"
#$priv sed --in-place   's|#include: "otherfile.conf"|include: "/etc/unbound/resolvconf.conf"|g' $newconf
$priv sed --in-place   's|# root-hints: ""|  root-hints: "/etc/unbound/root.hints"|g' $newconf
#$priv sed --in-place   's/# tls-system-cert: no/  tls-system-cert: yes/g' $newconf
#$priv sed --in-place   's|# tls-cert-bundle: ""|  tls-cert-bundle: /etc/ssl/certs/ca-certificates.crt|g' $newconf
$priv sed --in-place \
    '0,/# forward-zone:/{s/# forward-zone:/forward-zone: \
    name: "." \
    forward-tls-upstream: yes \
    # Quad9 \
    forward-addr: 9.9.9.9@853#dns.quad9.net \
    forward-addr: 2620:fe::fe@853#dns.quad9.net \
    forward-addr: 149.112.112.112@853#dns.quad9.net \
    forward-addr: 2620:fe::9@853#dns.quad9.net \
    # Cloudflare \
    forward-addr: 1.1.1.1@853#cloudflare-dns.com \
    forward-addr: 2606:4700:4700::1111@853#cloudflare-dns.com \
    forward-addr: 1.0.0.1@853#cloudflare-dns.com \
    forward-addr: 2606:4700:4700::1001@853#cloudflare-dns.com \
# forward-zone:/}' $newconf
echo -e "${BLUE}Unbound.conf after changes have taken place${NC}"
diff $ogconf $newconf
$priv mv $ogconf $oldconf
$priv mv $newconf $ogconf
echo
echo -e "${BLUE}Check unbound.conf for errors${NC}"
unbound-checkconf
echo
echo -e "${GREEN}Unbound.conf updated refreshing root.hints${NC}"
$priv curl --output $rootdir/root.hints https://www.internic.net/domain/named.cache
$priv systemctl enable unbound.service
$priv systemctl start unbound.service
$priv systemctl restart unbound.service
echo
echo -e "${BLUE}Unbound.service started checking DNSSEC validation${NC}"
unbound-host -C $ogconf -v sigok.verteiltesysteme.net
unbound-host -C $ogconf -v sigfail.verteiltesysteme.net
echo -e "${GREEN}Unbound.conf Updated VALIDATION COMPLETE${NC}\n"

