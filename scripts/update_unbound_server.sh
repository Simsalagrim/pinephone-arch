#!/bin/bash

#Unbound local server

source ./bash_colors

echo -e "${BLUE}Starting Update of Unbound.conf${NC}"
cd /etc/unbound/
echo -e "${BLUE}Proposed changes to unbound.conf${NC}"
diff ./unbound.conf ./unbound.conf.pacnew
echo
echo -e "${BLUE}Do you wish to update unbound.conf${NC}\n"
select update in Yes No
do 
    case $update in
        Yes)
            echo -e "${GREEN}Updating Unbound.conf...${NC}"
            break ;;
        No)
            echo -e "${RED}UPDATE NOT COMPLETE${NC}\n"
            exit 1 ;;
        *)
            echo -e "${RED}Invalid Input${NC}\n"
            continue ;;
    esac
done
doas sed -i 's/# num-threads: 1/  num-threads: 8/g' ./unbound.conf.pacnew
doas sed -i 's/# port: 53/  port: 53/g' ./unbound.conf.pacnew
doas sed -i 's/# so-rcvbuf: 0/  so-rcvbuf: 1m/g' ./unbound.conf.pacnew
doas sed -i 's/# msg-cache-size: 4m/  msg-cache-size: 256m/g' ./unbound.conf.pacnew
doas sed -i 's/# msg-cache-slabs: 4/  msg-cache-slabs: 16/g' ./unbound.conf.pacnew
doas sed -i 's/# rrset-cache-size: 4m/  rrset-cache-size: 500m/g' ./unbound.conf.pacnew
doas sed -i 's/# rrset-cache-slabs: 4/  rrset-cache-slabs: 16/g' ./unbound.conf.pacnew
doas sed -i 's/# cache-min-ttl: 0/  cache-min-ttl: 3600/g' ./unbound.conf.pacnew
doas sed -i 's/# cache-max-ttl: 86400/  cache-max-ttl: 86400/g' ./unbound.conf.pacnew
doas sed -i 's/# infra-cache-slabs: 4/  infra-cache-slabs: 16/g' ./unbound.conf.pacnew
doas sed -i 's/# do-ip4: yes/  do-ip4: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# do-ip6: yes/  do-ip6: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# do-udp: yes/  do-udp: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# do-tcp: yes/  do-tcp: yes/g' ./unbound.conf.pacnew
doas sed -i 's|# root-hints: ""|  root-hints: "/etc/unbound/root.hints"|g' ./unbound.conf.pacnew
doas sed -i 's/# hide-identity: no/  hide-identity: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# hide-version: no/  hide-version: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# harden-glue: yes/  harden-glue: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# harden-dnssec-stripped: yes/  harden-dnssec-stripped: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# use-caps-for-id: no/  use-caps-for-id: yes/g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: 10.0.0.0/8|  private-address: 10.0.0.0/8|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: 172.16.0.0/12|  private-address: 172.16.0.0/12|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: 192.168.0.0/16|  private-address: 192.168.0.0/16|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: 169.254.0.0/16|  private-address: 169.254.0.0/16|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: fd00::/8|  private-address: fd00::/8|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: fe80::/10|  private-address: fe80::/10|g' ./unbound.conf.pacnew
doas sed -i 's|# private-address: ::ffff:0:0/96|  private-address: ::ffff:0:0/96|g' ./unbound.conf.pacnew
doas sed -i 's/# private-domain: "example.com"/  private-domain: "home.lan"/g' ./unbound.conf.pacnew
doas sed -i 's/# unwanted-reply-threshold: 0/  unwanted-reply-threshold: 10000/g' ./unbound.conf.pacnew
doas sed -i 's/# do-not-query-localhost: yes/  do-not-query-localhost: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# prefetch: no/  prefetch: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# val-clean-additional: yes/  val-clean-additional: yes/g' ./unbound.conf.pacnew
doas sed -i 's/# key-cache-slabs: 4/  key-cache-slabs: 16/g' ./unbound.conf.pacnew
echo -e "${BLUE}Unbound.conf after changes have taken place${NC}"
diff ./unbound.conf ./unbound.conf.pacnew
doas mv ./unbound.conf ./unbound.conf.old
doas mv ./unbound.conf.pacnew ./unbound.conf
sleep 5
echo
echo -e "${GREEN}Unbound.conf updated refreshing root.hints${NC}"
doas curl --output /etc/unbound/root.hints https://www.internic.net/domain/named.cache
doas systemctl restart unbound.service
echo
echo -e "${BLUE}Unbound.service restarted checking DNSSEC validation${NC}"
unbound-host -C /etc/unbound/unbound.conf -v sigok.verteiltesysteme.net
unbound-host -C /etc/unbound/unbound.conf -v sigfail.verteiltesysteme.net
echo -e "${GREEN}Unbound.conf UPDATE COMPLETE${NC}\n"

