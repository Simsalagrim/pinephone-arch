#!/bin/bash

#YAY install script

source ./bash_colors

echo -e "${BLUE}Installing yay...${NC}"
sudo rm -r ~/Downloads/yay
git clone https://aur.archlinux.org/yay.git ~/Downloads/yay
cd ~/Downloads/yay
makepkg -si
sudo rm -r ~/Downloads/yay
echo -e "${GREEN}INSTALL COMPLETE${NC}\n"
