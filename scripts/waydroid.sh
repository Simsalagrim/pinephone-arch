#!/bin/bash

#Waydroid installer script

source ./bash_colors

#Waydroid Installer
echo -e "${BLUE}Installing Waydroid...${NC}"
sudo pacman -Syu
sudo pacman -S waydroid-image
sudo cp ../.config/my_configs/nftables-waydroid.conf /etc/nftables.conf
sudo systemctl restart nftables.service
sudo waydroid init -f 
sudo systemctl enable --now waydroid-container
sudo systemctl start --now waydroid-container
echo -e "${GREEN}WAYDROID INSTALLED...${NC}\n"

#F-Droid install for open source apps
#Aurora Store install for google play apps 
