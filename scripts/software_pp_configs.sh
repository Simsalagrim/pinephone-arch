#!/bin/bash

#Package configs

source ./bash_colors

#Desktop and Themes
echo -e "${BLUE}Installing Themes...${NC}"
for name in adapta-gtk-theme arc-gtk-theme pop-gtk-theme \
 arc-icon-theme pop-icon-theme
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
    elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           sudo pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
    else
        echo -e "${RED}$name was not installed.${NC}"
    fi
done
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"

#Add .vim .vimrc .bashrc and vim setup
echo -e "${BLUE}Generating Bash/Vim Configs...${NC}"
if pacman -Q | [[ $(grep -o "\bopendoas\b" 2>/dev/null) ]]
               then
                   2>/dev/null
             else
               echo -e "${RED}Check Installation of Depenedency opendoas${NC}"
               exit 1
             fi
sudo cp ../.config/my_configs/doas.conf /etc/doas.conf ;
cp ../.config/my_configs/.bashrc ~/.bashrc
cp ../.config/my_configs/.vimrc ~/.vimrc
cp -r ../.config/* ~/.config/
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir ~/.vim/plugged ~/.vim/undodir
vim -es -u ~/.vimrc -i NONE -c "PlugInstall" -c "qa"
echo -e "${GREEN}Bash/Vim Configs Generated${NC}\n"
