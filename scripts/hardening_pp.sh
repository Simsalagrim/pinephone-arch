#!/bin/bash

#Hardening script for pinephone

source ./bash_colors

echo -e "${BLUE}Checking Installation of Hardening Software...${NC}"
for name in arch-audit sysstat firejail lynis unbound \
 clamav rkhunter openresolv rng-tools opendoas 
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
      then
          echo  -e "${GREEN}Already Installed:  $name${NC}" 
          continue
      elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
      then
         sudo pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
                then
                echo -e "${GREEN}Installed:  $name${NC}"
              else
                echo -e "${RED}Check Installation of Depenedencies${NC}"
                exit 1
            fi
      else
            echo -e "${RED}$name was not installed.${NC}"
            exit 1
    fi
done
#Local
echo
echo -e "${GREEN}Hardening Software Installed${NC}\n"
echo -e "${BLUE}Generating Configs...${NC}"
sudo cp ../.config/my_configs/blacklist.conf /etc/modprobe.d/blacklist.conf
sudo cp ../.config/my_configs/issue /etc/issue
sudo cp ../.config/my_configs/issue /etc/issue.net
sudo cp ../.config/my_configs/login.defs /etc/login.defs
sudo cp ../.config/my_configs/limits.conf /etc/security/limits.conf
sudo cp ../.config/my_configs/profile /etc/profile
sudo cp -r ../.config/my_configs/sysctl.d/ /etc/
sudo sed -i 's/nullok/nullok        rounds=65536/g' /etc/pam.d/passwd 
sudo sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config
sudo chmod 600 /etc/ssh/ssh_config
sudo chmod 600 /etc/ssh/sshd_config
sudo chmod 600 /etc/ssh
echo -e "${GREEN}Configs Generated${NC}\n"

#Extra
echo -e "${BLUE}Starting Extra Services...${NC}"
echo -e "${BLUE}Starting Auditd Service${NC}"
sudo mkdir -p /etc/audit/rules.d
sudo cp ../.config/my_configs/audit.rules /etc/audit/rules.d/audit.rules
sudo systemctl enable auditd.service
sudo systemctl start auditd.service
echo -e "${GREEN}Auditd Service Started${NC}\n"
echo -e "${BLUE}Starting RNG Service${NC}"
sudo systemctl enable rngd.service
sudo systemctl start rngd.service
rngtest -c 1000 </dev/random
echo -e "${GREEN}RNG Service Started${NC}\n"
echo -e "${BLUE}Configuring FireJail${NC}"
sudo firecfg
echo -e "${GREEN}FireJail Configured${NC}\n"
echo -e "${BLUE}Starting Unbound Service${NC}"
sudo cp ../.config/my_configs/resolved.conf /etc/systemd/resolved.conf
sudo systemctl disable systemd-resolved.service
sudo systemctl mask systemd-resolved.service
sudo cp ../.config/my_configs/resolvconf.conf /etc/resolvconf.conf
sudo resolvconf -u
sudo cp /etc/unbound/unbound.conf /etc/unbound/unbound.conf.pacnew
source ./update_unbound_DoT.sh
echo -e "${BLUE}Starting ClamAV Service${NC}"
sudo freshclam
sudo systemctl enable clamav-freshclam.service
sudo systemctl start clamav-freshclam.service
sudo systemctl enable clamav-daemon.service
sudo systemctl start clamav-daemon.service
echo -e "${GREEN}ClamAV Service Started${NC}\n"
echo -e "${GREEN}ClamAV Definitions Check${NC}"
curl https://secure.eicar.org/eicar.com.txt | clamscan -
echo -e "${GREEN}CLAMAV VALIDATED${NC}\n"
echo -e "${BLUE}Starting RKHunter Root Check${NC}"
sudo rkhunter --update
sudo rkhunter --propupd
sudo cp ../.config/my_configs/rkhunter.conf.local /etc/rkhunter.conf.local
sudo rkhunter --config-check
echo -e "${GREEN}RKHUNTER VALIDATED${NC}\n"
echo -e "${BLUE}Starting Sysstat Service${NC}"
sudo systemctl enable sysstat.service
sudo systemctl start sysstat.service
echo -e "${GREEN}Sysstat Service Started${NC}\n"
#sudo systemctl enable fail2ban.service
#sudo systemctl start fail2ban.service

