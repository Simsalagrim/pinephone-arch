#!/bin/bash

#Package install script no.3

source ./bash_colors

echo -e  "${BLUE}Software Installation...${NC}"
for name in vlc keepassxc aisleriot gnuchess gnome-chess gnome-mines \
 arch-wiki-docs neofetch gnome-tetravex powerline powerline-fonts \
 quadrapassel
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
    elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           sudo pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
    else
        echo -e "${RED}$name was not installed.${NC}"
    fi
done  
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
