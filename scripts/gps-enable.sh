#!/bin/bash

#Enable GPS Services

source ./bash_colors

echo -e "${BLUE}Enabling GPS Services...${NC}"
mmcli -m 0 --location-set-gps-refresh-rate=0
mmcli -m 0 --location-enable-3gpp
mmcli -m 0 --location-enable-gps-nmea
mmcli -m 0 --location-enable-gps-raw
#mmcli -m 0 --location-enable-cdma-bs
#mmcli -m 0 --location-enable-agps-msa
mmcli -m 0 --location-enable-agps-msb
#mmcli -m 0 --location-enable-gps-unmanaged
mmcli -m 0 --location-set-enable-signal
echo -e "${GREEN}GPS Services Enabled...${NC}\n"
