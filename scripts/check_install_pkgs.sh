#!/bin/bash

#Checks installed packages

source ./bash_colors

echo -e  "${BLUE}Checking Installation...${NC}" ;
for name in vim base-devel firefox cmake git curl wget \
 man-db man-pages pam go nftables iptables-nft \
 arch-audit sysstat apparmor firejail lynis unbound logrotate \
 clamav rkhunter exa openresolv usbguard rng-tools \
 usbutils exfat-utils aspell-en vlc keepassxc aisleriot \
 gnuchess gnome-chess gnome-mines arch-wiki-docs neofetch gnome-tetravex \
 powerline powerline-fonts adapta-gtk-theme arc-gtk-theme pop-gtk-theme \
 arc-icon-theme pop-icon-theme
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
      then
          continue
      else pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; } 
          echo -e "${RED}$name was not installed.${NC}" ;
    fi
done  
echo -e  "${GREEN}Installation Checked!${NC}\n" ;
