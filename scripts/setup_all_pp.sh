#!/bin/bash

#Source all setup scripts

source ./bash_colors

echo -e "${BLUE}PinePhone64 Setup${NC}" ; \
#sudo useradd -mG wheel sanpo ; \
#echo '%wheel ALL=(ALL:ALL) ALL' | EDITOR='tee -a' visudo ; \
#echo 'Enter a passwd for user sanpo' ; \
#sudo passwd sanpo ; \
#login ; \
echo -e "${BLUE}Preparing Scripts...${NC}"
sudo rm /var/lib/pacman/db.lck 2>/dev/null ;
sudo chmod 750 ./nftables_pp.sh ./software1_pp.sh \
 ./software2_pp.sh ./software3_pp.sh ./check_install_pkgs.sh \
 ./software_pp_configs.sh ./hardening_pp.sh ./yay_pp.sh \
 ./gps-enable.sh ./gps-disable.sh
sudo chmod 740 ./usbguard_pp.sh
echo
echo -e "${BLUE}Updating Package List...${NC}"
sudo pacman -Sy --noconfirm --needed
echo -e "${GREEN}Package List Updated${NC}\n"
source ./nftables_pp.sh
echo -e "${BLUE}Upgrading Packages...${NC}"
sudo pacman -Syu --noconfirm --needed
echo -e "${GREEN}Packages Upgraded${NC}\n"
source ./software1_pp.sh
source ./software2_pp.sh
source ./software3_pp.sh
source ./software_pp_configs.sh
source ./check_install_pkgs.sh
source ./hardening_pp.sh
source ./gps-disable.sh
source ./yay_pp.sh
echo -e "${GREEN}SETUP COMPLETE${NC}"
