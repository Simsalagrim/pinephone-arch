#!/bin/bash

#Disable GPS Services

source ./bash_colors

echo -e "${BLUE}Disabling GPS Services...${NC}"
mmcli -m 0 --location-set-gps-refresh-rate=999999999
mmcli -m 0 --location-disable-3gpp
mmcli -m 0 --location-disable-gps-nmea
mmcli -m 0 --location-disable-gps-raw
mmcli -m 0 --location-disable-cdma-bs
mmcli -m 0 --location-disable-agps-msa
mmcli -m 0 --location-disable-agps-msb
mmcli -m 0 --location-disable-gps-unmanaged
mmcli -m 0 --location-set-disable-signal
echo -e "${GREEN}GPS Services Disabled...${NC}\n"
