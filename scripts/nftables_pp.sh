#!/bin/bash

#NFTables setup script

source ./bash_colors

echo -e "${BLUE}NFTables Installation...${NC}"
sudo pacman -S --noconfirm --needed nftables
yes | sudo pacman -S iptables-nft
sudo cp ../.config/my_configs/nftables.conf /etc/nftables.conf
echo
echo -e "${BLUE}Starting NFTables Service${NC}"
sudo systemctl enable nftables.service
sudo systemctl start nftables.service
echo -e "${GREEN}NFTables Service Started${NC}"
sudo nft list ruleset
echo -e "${GREEN}NFTABLES RULESET GENERATED${NC}\n"
