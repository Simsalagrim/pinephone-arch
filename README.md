# PinePhone    
A collection of repos, scripts and configs to setup pinephone and pinephonepro with a privacy and security focus.   

## Description    
All the required packages to do a fresh setup of pinephone or pinephonepro with Tow-Boot bootloader, dreemurrs-embedded Arch Linux Phosh on mobile, custom hardening setup scripts/configs and personal packages/configs. Almost everything is designed to be ran from the setup_pp.sh but all of the scripts will run independently. There are some scripts that have been left independent intentionally and require root to run, such as usbguard_pp.sh **(use with caution)**, or have been left out on purpose such as waydroid.sh *(I don't use it)*.   

This is a privacy and security focused build so things like GPS, WiFi, and Bluetooth have been disabled in the software as well as other hardening measures. If you wish to re-enable these there is a script to toggle the GPS and alterations can be  made to the config files to suit your needs. I have made this repo for myself as such it contains my personal configs for .bashrc, .vimrc, etc. which will replace the ones on the system so I suggest you read the scripts and configs before proceeding with the installation.   

## Installation       
### Tow-Boot Installation      
###### PinePhone    
* wget https://github.com/Tow-Boot/Tow-Boot/releases/download/release-2021.10-005/pine64-pinephoneA64-2021.10-005.tar.xz   
Write to SD Card   
* tar -xvf pine64-pinephoneA64-2021.10-005.tar.xz
* gnome-disks to restore mmcboot.installer.img to the SD card   
* Insert SD card into device and start should boot from tow-boot.img on SD Card   
* Install Tow-Boot to eMMC   
* Power off and remove SD card    

###### PinePhonePro      
* wget https://github.com/Tow-Boot/Tow-Boot/releases/download/release-2021.10-005/pine64-pinephonePro-2021.10-005.tar.xz   
Write to SD Card   
* tar -xvf pine64-pinephonePro-2021.10-005.tar.xz   
* gnome-disks to restore spi.installer.img to the SD card   
* Insert SD card into device and start holding down the reset button on the back of the phone this should boot from tow-boot.img on SD Card    
* Install Tow-Boot to SPI   

### Flash Arch to eMMC      
###### PinePhone       
* wget https://github.com/dreemurrs-embedded/Pine64-Arch/releases/download/20220502/archlinux-pinephone-phosh-20220502.img.xz   
Write to and from SD card   
*Make sure you select the correct device eg. /dev/mmcblk0 /dev/mmcblk1 etc.*   
* unxz archlinux-pinephone-phosh-20220502.img.xz   
* gnome-disks to restore archlinux-pinephone-phosh-20220502.img to the SD card   
* dd if=/dev/mmcblkX of=/dev/mmcblkY bs=1M oflag=sync status=progress   

Note: dd will duplicate the SD card to the eMMC exactly, this includes the size of the SD card used, you will need to extend the eMMc partition using a tool such as gparted or gnome-disks to make full use of the eMMC.

###### PinePhonePro      
* wget https://github.com/dreemurrs-embedded/Pine64-Arch/releases/download/20220502/archlinux-pinephone-pro-phosh-20220502.img.xz   
Write to and from SD card   
*Make sure you select the correct device eg. /dev/mmcblk0 /dev/mmcblk1 etc.*   
* unxz archlinux-pinephone-pro-phosh-20220502.img.xz   
* gnome-disks to restore archlinux-pinephone-pro-phosh-20220502.img to the SD card   
* dd if=/dev/mmcblkX of=/dev/mmcblkY bs=1M oflag=sync status=progress   

Note: dd will duplicate the SD card to the eMMC exactly, this includes the size of the SD card used, you will need to extend the eMMc partition using a tool such as gparted to make full use of the eMMC.

### Setup Scripts      
* git clone https://gitlab.com/Simsalagrim/pinephone-arch.git
* cd pinephone/setup   
* sudo chmod 750 setup_pp.sh   
* ./setup_pp.sh   

###### Issues    
* If there is a package missing or an error during the install run the setup_pp.sh again.   
* If your screen rotation is not working after updating add IgnorePkg = iio-sensor-proxy /etc/pacman.conf or by downgrading the package after install.    
* Alternativley stop the service with sudo systemctl stop iio-sensor-proxy.service.    
* You can also detect sensor running gdbus introspect --system --dest net.hadess.SensorProxy --object-path /net/hadess/SensorProxy and monitor-sensor to check accelerometer change.    
* To modify the setenv bootargs edit the /boot/boot.txt and run /boot/mkscr    
* The default scale for the screen is 200% if you prefer a smaller scale settings can be changed temporarily in gnome tweaks or to permanently change the default screen scaling edit /usr/share/phosh/phoc.ini to include:   
    
....    
[output:DSI-1]    
\# scale = 2     
scale = 1.5   
....
               
## Keyboard    
If you are using the pp-keyboard case there is an issue with the pine key not working after a certain version of the kernel. This can be fixed by changing the keymap to use AltG in place of the pine key to access the special characters under the first row numbers (AltG + 1 to |, AltG + 2 to \ and so on). To change this key-mapping you need to do the following.
* sudo systemctl edit phosh.service 
* In the space provided at the top between # symbols add the following:    

\#\#\#    

[Service]
Environment=XKB_DEFAULT_MODEL=ppkb    
 
\#\#\#    
* May need to reset AltG as alternate character key in settings, keyboard
* Restart the pinephone for changes to take affect

If the keyboard seems to be charging slowly check to see if the /sys/class/power_supply/axp20x-usb/input_current_limit has been set to allow full current throughput of 1500mA, the default setting is 500mA. To set the limit to 1500mA you can manually edit the file by using the echo command.
* echo 1500000 | sudo tee /sys/class/power_supply/axp20x-usb/input_current_limit.

or

* If you want a daemon to manage the battery input current one can be found at https://gitlab.com/undef1/kbd/-/tree/master

There is a FAQ for the keyboard that can be found at https://xnux.eu/pinephone-keyboard/faq.html that list most of the common problems and fixes for the keyboard including the ones above. Whilst the key-mapping fix works perfectly I have found managing the battery levels together somewhat ineffective and tend to take the phone out of the case, charging the keyboard and phone individually. The battery can be disconnected with a double press of the power button and re-connected with a single press to allow reserving for when needed.

## Roadmap    
* Include a custom installer image.
* Include Matrix with sms/mms bridge
* Include Hardened-malloc

## License    
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007   

## Project status    
Currently under development   

## Repos    
https://github.com/Tow-Boot/Tow-Boot   
https://github.com/dreemurrs-embedded/Pine64-Arch    
https://xnux.eu/pinephone-keyboard/faq.html    
https://gitlab.com/undef1/kbd/-/tree/master    
https://wiki.mobian-project.org/doku.php?id=tweaks#scale-the-screen    
